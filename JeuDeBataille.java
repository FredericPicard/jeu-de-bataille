import java.util.ArrayList;

public class JeuDeBataille {
    private ArrayList<Joueur> joueurs;
    ArrayList<Carte> jeu52Cartes;
    ArrayList<Carte> paquetJ1;
    ArrayList<Carte> paquetJ2;


    public JeuDeBataille() {


        //creer les joueurs
        joueurs = new ArrayList<Joueur>();
        paquetJ1 = new ArrayList<Carte>();
        paquetJ2 = new ArrayList<Carte>();
        joueurs.add(new Joueur("toto", paquetJ1));
        joueurs.add(new Joueur("titi", paquetJ2));


        // creer les cartes
        jeu52Cartes = new ArrayList<Carte>();

        for (int i = 1; i <= 4; i++) {
            String couleur;
            if (i == 1) {
                couleur = "coeur";
            } else if (i == 2) {
                couleur = "carreau";
            } else if (i == 3) {
                couleur = "pique";
            } else {
                couleur = "trefle";
            }
            jeu52Cartes.add(new Carte("2 de " + couleur, 2));
            jeu52Cartes.add(new Carte("3 de " + couleur, 3));
            jeu52Cartes.add(new Carte("4 de " + couleur, 4));
            jeu52Cartes.add(new Carte("5 de " + couleur, 5));
            jeu52Cartes.add(new Carte("6 de " + couleur, 6));
            jeu52Cartes.add(new Carte("7 de " + couleur, 7));
            jeu52Cartes.add(new Carte("8 de " + couleur, 8));
            jeu52Cartes.add(new Carte("9 de " + couleur, 9));
            jeu52Cartes.add(new Carte("10 de " + couleur, 10));
            jeu52Cartes.add(new Carte("valet de " + couleur, 11));
            jeu52Cartes.add(new Carte("dame de " + couleur, 12));
            jeu52Cartes.add(new Carte("roi de " + couleur, 13));
            jeu52Cartes.add(new Carte("As de " + couleur, 14));
        }

//       //-----------------test creation des carte
//        int i=0;
//        for (Carte j: jeu52Cartes) {
//         i++;
//          System.out.println(i+"  "+j.getTitre()+"  valeur : "+j.getValeur());    //verifie la creation des carte
//        }
//       // ----------------fin du test
}

    public void jouerUnePartie() {


      Paquet jeu52= new Paquet(jeu52Cartes);

      jeu52.distribue(paquetJ1, paquetJ2);

//        //-----------------test creation des carte
//        int k=0;
//
//        for (Carte j: paquetJ1) {
//            k++;
//            System.out.println(k+"  "+j.getTitre()+"  valeur : "+j.getValeur());    //verifie la creation des carte
//        }
//        // ----------------fin du test

        while (paquetJ1.size()!=0 || paquetJ2.size()!=0) {
            tourDeJeu();
            if (paquetJ1.size()==0) {
                System.out.println(joueurs.get(1).getNom()+"  gagne!");
            break;
            }
            else if (paquetJ2.size()==0) {
                System.out.println(joueurs.get(0).getNom()+"  gagne!");
            break;
            }
        }

    }


    public void tourDeJeu() {
        Paquet paquet1= new Paquet(paquetJ1);
        Paquet paquet2= new Paquet(paquetJ2);

        // le joueur 1 tire une carte
        Carte carte1=joueurs.get(0).pioche(paquet1);

        // le joueur 2 tire une carte
        Carte carte2=joueurs.get(1).pioche(paquet2);

//        System.out.println(joueurs.get(0).getNom()+" pioche : " +carte1.getTitre()+"  "+joueurs.get(1).getNom()+ " pioche : "+carte2.getTitre());

        if (carte1.estPlusForteQue(carte2)) {
            joueurs.get(0).gagne(paquet1,carte1);
            joueurs.get(0).gagne(paquet1,carte2);
//             System.out.println(joueurs.get(0).getNom()+" gagne ! " + "il lui reste" + paquetJ1.size()+" cartes"+" et "+paquetJ2.size()+" cartes a son adversaire");
        }
         else  if  (carte2.estPlusForteQue(carte1)) {
            joueurs.get(1).gagne(paquet2,carte1);
            joueurs.get(1).gagne(paquet2,carte2);
//            System.out.println(joueurs.get(1).getNom()+" gagne ! " + "il lui reste" + paquetJ2.size()+" cartes"+" et "+paquetJ1.size()+" cartes a son adversaire");
        }
            else {
            joueurs.get(0).gagne(paquet1,carte1);
            joueurs.get(1).gagne(paquet2,carte2);
//            System.out.println(" égalité ! ");
        }

    }



    // prendre une carte au hazard


    // joueur une partie

    // joueur un tour
}
