import java.util.ArrayList;

public class Joueur {
   private String nom;
   private ArrayList<Carte> paquet;

   //private Paquet paquet;

   public Joueur (String nom, ArrayList<Carte> paquet) {
       this.nom=nom;
       this.paquet=paquet;
   }


 // tirer une carte de son paquet
    public Carte pioche(Paquet paquet) {
    Carte carte=paquet.prend();
    return carte;
    }

 // gagner une carte
 public void gagne(Paquet paquet,Carte carte) {
     paquet.ajoute(carte);
 }

  public String getNom() {
      return nom;
  }
}
