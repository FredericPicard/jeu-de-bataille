import java.util.ArrayList;

public class Paquet {

    private ArrayList<Carte> paquet;

    public Paquet(ArrayList<Carte> paquet) {
        this.paquet = paquet;
    }


    // prendre la premiere carte
    public Carte prend() {
        Carte carte=paquet.get(0);
        paquet.remove(0);
        return carte;
    }

    // ajouter une carte a la fin
    public void ajoute(Carte carteAjoute) {
        paquet.add(carteAjoute);
    }


    // prendre une carte au hazard et la distribuer
    public void distribue(ArrayList<Carte> paquet1,ArrayList<Carte> paquet2) {
        Carte carte;
        boolean i=true;
        System.out.println("taille du paquet: "+ paquet.size() + "  "+ paquet1.size()+" "+ paquet2.size());
        while (paquet.size()>0) {
        int numeroCarte = (int) (Math.random() * paquet.size());

        carte = paquet.get(numeroCarte);
            if (i) {paquet1.add(carte);
                System.out.print("carte joueur1: "+ carte.getTitre() + "                  ");
            }
                else {paquet2.add(carte);
                System.out.println("carte joueur2: " + carte.getTitre() + "  ");
            }
        paquet.remove(numeroCarte);
        i=!i;
        }
    }


}

