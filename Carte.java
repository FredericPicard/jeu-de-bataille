
public class Carte {

    private String titre;
    private int puissance;


    public Carte(String titre, int puissance) {
        this.titre = titre;
        this.puissance = puissance;
    }

    // test de la puissance d'une carte par rapprot a une autre

    public boolean estPlusForteQue(Carte uneAutreCarte) {
        return puissance > uneAutreCarte.puissance;
    }
    // renvoie le nom de la carte

    public String getTitre() {
        return titre;
    }
    public int getValeur() {
        return puissance;
    }


}


